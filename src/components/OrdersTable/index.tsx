import React, { FC } from "react";
import { useWindowSize } from "hooks/useWindowSize";
import { getRowsNumber, getRowBackground } from "./utils";
import { FeedType, Order } from "types/orderbook";

import styles from "./index.module.css";

export interface Props {
  data: Order[];
  feedType: FeedType;
}

const OrdersTable: FC<Props> = ({ data, feedType }) => {
  const { width, height } = useWindowSize();
  const rows = data.slice(0, getRowsNumber(width, height));

  const getCn = (section: string) => `${styles[`${feedType}${section}`]}`;
  const headerClasses = `${styles.header} ${getCn("Header")}`;
  const contentClasses = `${styles.content} ${getCn("Content")}`;
  const priceClasses = `${styles.value} ${getCn("Price")}`;
  const rowClasses = `${styles.row} ${getCn("Row")}`;

  return (
    <div className={styles.root}>
      <header className={headerClasses}>
        <span className={styles.headerItem}>PRICE</span>
        <span className={styles.headerItem}>SIZE</span>
        <span className={styles.headerItem}>TOTAL</span>
      </header>
      <div className={contentClasses}>
        {rows.map(([price, size, total = 0]) => {
          const percentage = (total / (data[data.length - 1][2] || 0)) * 100;
          const style = getRowBackground(percentage, width, feedType);
          return (
            <div key={price} style={style} className={rowClasses}>
              <span className={priceClasses}>{price}</span>
              <span className={styles.value}>{size}</span>
              <span className={styles.value}>{total}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default OrdersTable;
