import { FeedType, FeedTypes } from "types/orderbook";

export const getRowBackground = (
  percentage: number,
  screenWidth: number,
  feedType: FeedType
) => {
  const isAsks = feedType === FeedTypes.asks;
  const isMobile = screenWidth <= 600;
  const direction = (!isAsks && isMobile) || isAsks ? "to right" : "to left";
  return {
    background: `linear-gradient(${direction}, ${
      isAsks ? "rgba(227,65,93,0.3)" : "rgba(81,156,88,0.3)"
    } ${percentage}%,  rgba(255,255,255) ${percentage}%, rgba(2,0,36,0) ${percentage}%)`,
  };
};

export const getRowsNumber = (width: number, height: number) => {
  const headersAndFooterHeight = 140;
  const mobileHeader = 40;
  const rowHeight = 30;
  const tableContentHeight =
    width > 600
      ? height - headersAndFooterHeight
      : (height - headersAndFooterHeight - mobileHeader) / 2;
  return Math.floor(tableContentHeight / rowHeight);
};
