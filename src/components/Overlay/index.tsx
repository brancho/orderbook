import React, { FC } from "react";

import styles from './index.module.css'

const Overlay: FC = ({ children }) => (
  <div className={styles.root}>
    <main className={styles.main}>{children}</main>
  </div>
);

export default Overlay;
