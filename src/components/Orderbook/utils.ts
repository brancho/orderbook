import { Order } from "types/orderbook";

export const getSpread = (asks: Order[], bids: Order[]) => {
  if (!(asks.length && bids.length)) return { spread: 0, spreadPercent: "" };
  const topAsk = asks[0][0];
  const topBid = bids[0][0];
  const spread = Math.round((topAsk - topBid) * 100) / 100;
  const spreadPercent = getPercent(spread / topAsk);
  return { spread, spreadPercent };
};

const getPercent = (num: number) =>
  num.toLocaleString("en", {
    style: "percent",
    minimumFractionDigits: 2,
  });
