import { useCallback, useEffect, useState } from "react";
import OrdersTable from "components/OrdersTable";
import Overlay from "components/Overlay";
import { useOrders } from "hooks/useOrders";
import { getSpread } from "./utils";
import { ETHUSD, XBTUSD } from "constants/orderbook";
import { FeedTypes } from "types/orderbook";

import styles from "./index.module.css";

const OrderBook = () => {
  const [product, setProduct] = useState(XBTUSD);
  const [ordersPaused, setOrdersPaused] = useState(false);
  const { orderbook, error } = useOrders(product, ordersPaused);
  const { asks, bids } = orderbook;

  useEffect(() => {
    document.addEventListener("visibilitychange", onVisibilityChange);
    return () =>
      document.removeEventListener("visibilitychange", onVisibilityChange);
  }, []);

  const onVisibilityChange = useCallback(() => setOrdersPaused(true), []);
  const toggleProduct = useCallback(
    () => setProduct(product === XBTUSD ? ETHUSD : XBTUSD),
    [product]
  );
  const { spread, spreadPercent } = getSpread(asks, bids);

  const PauseOverlay = () =>
    ordersPaused ? (
      <Overlay>
        <p>Feed has been paused.</p>
        <button
          className={styles.button}
          onClick={() => setOrdersPaused(false)}
        >
          Click to continue
        </button>
      </Overlay>
    ) : null;

  const ErrorOverlay = () =>
    error ? (
      <Overlay>
        <p>An error has occurred. Try again later.</p>
      </Overlay>
    ) : null;

  const MobileMidsection = () => (
    <header className={`${styles.header} ${styles.mobileHeader}`}>
      <span className={styles.spread}>
        Spread: {spread} ({spreadPercent})
      </span>
    </header>
  );

  return (
    <div className={styles.root}>
      <header className={styles.header}>
        <span>Order Book</span>
        <span className={`${styles.spread} ${styles.topSpread}`}>
          Spread: {spread} ({spreadPercent})
        </span>
        <span>{product}</span>
      </header>
      <main className={styles.main}>
        <OrdersTable data={bids} feedType={FeedTypes.bids} />
        <MobileMidsection />
        <OrdersTable data={asks} feedType={FeedTypes.asks} />
      </main>
      <footer className={styles.footer}>
        <button className={styles.button} onClick={toggleProduct}>
          Toggle feed
        </button>
      </footer>
      <PauseOverlay />
      <ErrorOverlay />
    </div>
  );
};

export default OrderBook;
