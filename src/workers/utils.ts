import { FeedTypes, Orderbook, FeedType, Order } from "types/orderbook";

export const getInitialOrUpdatedOrderBook = (
  orderbook: Orderbook | null,
  newAsks: Order[],
  newBids: Order[]
) => {
  if (orderbook === null)
    return { asks: addTotals(newAsks), bids: addTotals(newBids) };

  return {
    asks: getUpdatedOrders(orderbook.asks, newAsks, FeedTypes.asks),
    bids: getUpdatedOrders(orderbook.bids, newBids, FeedTypes.bids),
  };
};

export const getUpdatedOrders = (
  currentOrders: Order[],
  updates: Order[],
  feedType: FeedType
) => {
  let orders = [...currentOrders];

  updates.forEach((orderUpdate) => {
    const [price, size] = orderUpdate;
    const updatePriceInFeedIndex = orders.findIndex(
      (item) => item[0] === price
    );
    const hasPrice = updatePriceInFeedIndex > -1;
    if (!size) {
        hasPrice && orders.splice(updatePriceInFeedIndex, 1);
    } else if (hasPrice) {
      orders[updatePriceInFeedIndex] = orderUpdate;
    } else {
      orders = getFeedWithNewOrder(orders, orderUpdate, feedType);
    }
  });

  return addTotals(orders);
};

const getFeedWithNewOrder = (
  currentFeed: Order[],
  orderUpdate: Order,
  feedType: FeedType
) => {
  const feed = [...currentFeed];
  const insertIndex = feed.findIndex(([price]) =>
    feedType === FeedTypes.bids
      ? price < orderUpdate[0]
      : price > orderUpdate[0]
  );
  if (insertIndex === -1) {
    feed.push(orderUpdate);
  } else if (insertIndex === 0) {
    feed.unshift(orderUpdate);
  } else {
    feed.splice(insertIndex, 0, orderUpdate);
  }
  return feed;
};

export const addTotals = (orders: Order[]) => {
  let total = 0;
  return orders.map(([price, size]) => {
    total += size;
    return [price, size, total];
  }) as Order[];
};

export const composeMessage = (event: string, product: string) =>
  JSON.stringify({
    event,
    feed: "book_ui_1",
    product_ids: [`PI_${product}`],
  });
