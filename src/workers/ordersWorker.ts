import { composeMessage, getInitialOrUpdatedOrderBook } from "./utils";
import {
    Orderbook,
    OrderBookAction,
    OrderBookEvent,
} from "types/orderbook";

const url = "wss://www.cryptofacilities.com/ws/v1";

(function OrderWorker() {
  const socket = new WebSocket(url);
  let currentProduct = "";
  let orderbook: Orderbook | null = null;
  let updatesInterval: ReturnType<typeof setInterval>;

  onmessage = e => {
    const { action, product } = e.data;
    if (action === OrderBookAction.subscribe) subscribe(product);
    if (action === OrderBookAction.closeSocket) closeSocket();
  };

  socket.onopen = () => {
    postMessage({ event: OrderBookEvent.ready });
  };

  socket.onmessage = e => {
    const { event, asks, bids, message: error } = JSON.parse(e.data);
    if (event === OrderBookEvent.subscribed) {
      orderbook = null;
      startUpdatesStream();
    }
    if (event === OrderBookEvent.error) onError(error);
    if (asks) orderbook = getInitialOrUpdatedOrderBook(orderbook, asks, bids);
  };

  const subscribe = (product: string) => {
    if (currentProduct) {
      clearInterval(updatesInterval);
      socket.send(composeMessage(OrderBookAction.unsubscribe, currentProduct));
    }
    currentProduct = product;
    socket.send(composeMessage(OrderBookAction.subscribe, product));
  };

  const startUpdatesStream = () => {
    updatesInterval = setInterval(() => {
      postMessage({
        event: OrderBookEvent.updated,
        orderbook
      });
    }, 500);
  };

  const onError = (error: string) => {
    postMessage({
      event: OrderBookEvent.error,
      error,
    });
    socket.send(composeMessage(OrderBookAction.unsubscribe, currentProduct));
    closeSocket();
  };

  const closeSocket = () => {
    clearInterval(updatesInterval);
    socket.close();
  };
})();

export default {};
