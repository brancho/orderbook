type Price = number;
type Size = number;
type Total = number;

export type Order = [Price, Size, Total?];

export interface Orderbook {
  asks: Order[];
  bids: Order[];
}

export enum OrderBookAction {
    subscribe = "subscribe",
    unsubscribe = "unsubscribe",
    closeSocket = "closeSocket",
}

export enum OrderBookEvent {
    ready = "ready",
    subscribed = "subscribed",
    updated = "updated",
    error = "error",
}

export enum FeedTypes {
  bids = "bids",
  asks = "asks",
}

export type FeedType = keyof typeof FeedTypes;
