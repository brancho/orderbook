import React from "react";
import { getSpread } from "components/Orderbook/utils";
import { FeedTypes, Order, Orderbook } from "types/orderbook";
import {
  getRowBackground,
  getRowsNumber,
} from "components/OrdersTable/utils";
import {getInitialOrUpdatedOrderBook} from "workers/utils";
import {mockOrderbook} from "mocks";

describe("utility functions for modifying the data and rendering", () => {
  it("getSpread", () => {
    const asks: Order[] = [
      [1000, 1, 1],
      [1001, 1, 2],
    ];
    const bids: Order[] = [
      [999, 1, 1],
      [998, 1, 2],
    ];
    const output = { spread: 1, spreadPercent: "0.10%" };

    expect(getSpread(asks, bids)).toEqual(output);
  });

  it("getRowBackground", () => {
    const percentage = 30;
    const screenWidth = 1000;
    const feedType = FeedTypes.bids;
    const output = {
      background:
        "linear-gradient(to left, rgba(81,156,88,0.3) 30%,  rgba(255,255,255) 30%, rgba(2,0,36,0) 30%)",
    };

    expect(getRowBackground(percentage, screenWidth, feedType)).toEqual(output);
  });

  it("getRowsNumber", () => {
    const width = 1000;
    const height = 500;
    const output = 12;

    expect(getRowsNumber(width, height)).toEqual(output);
  });

  it("getInitialOrUpdatedOrderBook - returns orderbook snapshot with totals", () => {
    const orderbook = null;

    const newAsks: Order[] = [
      [1005, 1],
      [1006, 1],
      [1007, 1],
      [1008, 1],
      [1009, 1],
    ];
    const newBids: Order[] = [
      [1000, 1],
      [999, 1],
      [998, 1],
      [997.5, 1],
      [997, 1],
    ];
    const output = mockOrderbook;

    expect(getInitialOrUpdatedOrderBook(orderbook, newAsks, newBids)).toEqual(output);
  });

    it("getInitialOrUpdatedOrderBook - adds new orders with totals", () => {
        const orderbook: Orderbook = {
            bids: [
                [999, 1, 1],
                [998, 1, 2],
                [997.5, 1, 3],
                [997, 1, 4],
            ],
            asks: [
                [1005, 1, 1],
                [1006, 1, 2],
                [1008, 1, 3],
                [1009, 1, 4],
            ],
        };
        const newAsks: Order[] = [
            [1007, 1],
        ];
        const newBids: Order[] = [
            [1000, 1],
        ];
        const output = mockOrderbook;

        expect(getInitialOrUpdatedOrderBook(orderbook, newAsks, newBids)).toEqual(output);
    });

    it("getInitialOrUpdatedOrderBook - removes orders with 0 size", () => {
        const orderbook: Orderbook = mockOrderbook;
        const newAsks: Order[] = [
            [1005, 0],
            [1006, 0],
            [1007, 0],
            [1008, 0]
        ];
        const newBids: Order[] = [
            [1000, 0],
            [999, 0],
            [998, 0],
            [997.5, 0],
        ];
        const output = {
            bids: [
                [997, 1, 1],
            ],
            asks: [
                [1009, 1, 1],
            ],
        };

        expect(getInitialOrUpdatedOrderBook(orderbook, newAsks, newBids)).toEqual(output);
    });
});
