import React from "react";
import OrdersTable from "components/OrdersTable";
import { render, screen } from "@testing-library/react";
import { FeedTypes, Order } from "types/orderbook";

describe("OrdersTable", () => {
  const bids: Order[] = [
    [1000, 5, 5],
    [999, 6, 11],
    [998, 1, 12],
    [997.5, 3, 15],
    [997, 2, 17],
  ];

  it("renders table header", () => {
    const { getByText } = render(
      <OrdersTable feedType={FeedTypes.bids} data={bids} />
    );
    const price = getByText(/price/i);
    const size = getByText(/size/i);
    const total = getByText(/total/i);
    expect(price).toBeInTheDocument();
    expect(size).toBeInTheDocument();
    expect(total).toBeInTheDocument();
  });

  it("renders table values", () => {
    render(<OrdersTable feedType={FeedTypes.bids} data={bids} />);
    const price = screen.getByText(/999/i);
    const size = screen.getByText(/6/i);
    const total = screen.getByText(/17/i);
    expect(price).toBeInTheDocument();
    expect(size).toBeInTheDocument();
    expect(total).toBeInTheDocument();
  });
});
