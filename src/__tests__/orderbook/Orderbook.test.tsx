import React from "react";
import OrderBook from "components/Orderbook";
import { render, screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";

describe("OrderBook", () => {
  it("renders header", () => {
    render(<OrderBook />);
    const title = screen.getByText(/order book/i);
    expect(title).toBeInTheDocument();
  });

  it("renders footer", () => {
    render(<OrderBook />);
    const button = screen.getByText(/toggle feed/i);
    expect(button).toBeInTheDocument();
  });

  it("renders pause overlay", () => {
    render(<OrderBook />);
    const button = screen.getByText(/click to continue/i);
    expect(button).not.toBeInTheDocument();

    act(() => {
      Object.defineProperty(document, "visibilityState", { value: "hidden" });
      fireEvent(document, new Event("visibilitychange"));
    });
      expect(button).toBeInTheDocument();
  });
});
