import {Orderbook} from "types/orderbook";

export const mockOrderbook: Orderbook = {
    bids: [
        [1000, 1, 1],
        [999, 1, 2],
        [998, 1, 3],
        [997.5, 1, 4],
        [997, 1, 5],
    ],
    asks: [
        [1005, 1, 1],
        [1006, 1, 2],
        [1007, 1, 3],
        [1008, 1, 4],
        [1009, 1, 5],
    ],
};
