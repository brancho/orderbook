import { useEffect, useState } from "react";

type WindowSize = { width: number; height: number };
export const useWindowSize = () => {
  const [size, setSize] = useState<WindowSize>(getWindowSize());

  useEffect(() => {
    const handleResize = () => setSize(getWindowSize());
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return size;
};

const getWindowSize = () => ({
  width: window.innerWidth,
  height: window.innerHeight,
});
