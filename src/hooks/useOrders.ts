import { useEffect, useState, useRef, useCallback } from "react";
// eslint-disable-next-line
import Worker from "worker-loader!workers/ordersWorker.ts";
import { Orderbook, OrderBookAction, OrderBookEvent } from "types/orderbook";

const initialOrderbook: Orderbook = {
  bids: [],
  asks: [],
};

export const useOrders = (product: string, ordersPaused = false) => {
  const worker = useRef<Worker | null>(null);
  const [orderbook, setOrderbook] = useState(initialOrderbook);
  const [error, setError] = useState("");

  const subscribe = useCallback(() => {
    if (!worker.current) {
      worker.current = new Worker();
      worker.current.onmessage = ({ data: { event, orderbook, error } }) => {
        if (event === OrderBookEvent.ready) getOrderbook(product);
        if (event === OrderBookEvent.updated) setOrderbook(orderbook);
        if (event === OrderBookEvent.error) setError(error);
      };
    }
  }, [worker, product]);

  const unsubscribe = useCallback(() => {
    worker.current?.postMessage({ action: OrderBookAction.closeSocket });
    worker.current = null;
  }, [worker]);

  const getOrderbook = useCallback(
    (product: string) => {
      worker.current?.postMessage({
        action: OrderBookAction.subscribe,
        product,
      });
    },
    [worker]
  );

  useEffect(() => {
    if (worker.current) getOrderbook(product);
  }, [product]);

  useEffect(() => {
    if (!ordersPaused) subscribe();
    return () => unsubscribe();
  }, [ordersPaused]);

  return { orderbook, error };
};
