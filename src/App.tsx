import React from "react";
import OrderBook from "components/Orderbook";

function App() {
  return (
    <div>
      <OrderBook />
    </div>
  );
}

export default App;
